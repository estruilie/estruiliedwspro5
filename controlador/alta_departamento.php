<?php

error_reporting(E_ALL);
ini_set('display_errors','1');

include("recoge.php");
include_once "../modelo/modelo.php";

$nombre = recoge("nombre");
$ubicacion = recoge("ubicacion");

$departamento = new Departamento($nombre, $ubicacion);

$modelo = new Modelo();
$modelo->guardarDepartamento($departamento);

header("Location: ../vista/operacionOK.php");
?>
