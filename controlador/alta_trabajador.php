<?php

error_reporting(E_ALL);
ini_set('display_errors','1');

include("recoge.php");
include_once "../modelo/modelo.php";

$id = recoge("id");
$nombre = recoge("nombre");
$departamento = recoge("departamento");

$trabajador = new Trabajador($id, $nombre, $departamento);

$modelo = new Modelo();
$modelo->guardarTrabajador($trabajador);

header("Location: ../vista/operacionOK.php");
?>
