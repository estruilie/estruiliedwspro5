<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

class Departamento {

    // atributos
    private $nombre;
    private $ubicacion;


    // constructor
    public function __construct($nombre, $ubicacion) {
        $this->nombre = $nombre;
        $this->ubicacion = $ubicacion;
    }

    // getters
    public function getNombre() {
        return $this->nombre;
    }

    public function getUbicacion() {
        return $this->ubicacion;
    }
    
    // setters
    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setUbicacion($ubicacion) {
        $this->ubicacion = $ubicacion;
    }

}

?>