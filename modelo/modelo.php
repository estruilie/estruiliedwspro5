<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
require_once("departamento.php");
require_once("trabajador.php");

/* 
 * ESTA CLASE PERMITIRÁ EXTRAER LA INFORMACIÓN NECESARIA DE LAS CLASES DEPARTAMENTO Y TRABAJADOR
 */

class Modelo {
    
    // atributos
    private $ficheroDep = "../departamentos.txt"; 
    private $ficheroTra = "../trabajadores.txt";
    
    // métodos
    function leerDepartamentos() {

        if ( !is_file($this->ficheroDep) ) {  
            return;
        }
        
        $arrayDepartamentos = file($this->ficheroDep);
        
        foreach ($arrayDepartamentos as $departamento) { //extraemos cada línea del fichero
            $dep = explode(";",$departamento);  // generamos un array de strings separados por ,
            $d = new Departamento($dep[0],$dep[1]); // creamos el objeto departamento
            $departamentos[] = $d; // lo añadimos al array de departamentos
        }
        
        return $departamentos;
        
    }
     
    function guardarDepartamento($departamento) {
        
        $fich = fopen($this->ficheroDep, "a");
        fwrite($fich, $departamento->getNombre() . ';' . $departamento->getUbicacion() . PHP_EOL);
        fclose($fich);
        
    }
    
    function leerTrabajadores() {
       
        if ( !is_file($this->ficheroTra) ) {  
            return;
        }
        
        $arrayTrabajadores = file($this->ficheroTra);
        
        foreach ($arrayTrabajadores as $trabajador) { //extraemos cada línea del fichero
            $tra = explode(";",$trabajador);  // generamos un array de strings separados por ,
            $t = new Trabajador($tra[0],$tra[1],$tra[2]); // creamos el objeto trabajador
            $trabajadores[] = $t; // lo añadimos al array de trabajadores
        }
        
        return $trabajadores;
        
    }
    
    function guardarTrabajador($trabajador) {
        
        $fich = fopen($this->ficheroTra, "a");
        fwrite($fich, $trabajador->getId() . ';' . $trabajador->getNombre() .  ';' .$trabajador->getDepartamento() . PHP_EOL);
        fclose($fich);
        
    }
}