<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

class Trabajador {

    // atributos
    private $id;
    private $nombre;
    private $departamento;


    // constructor
    public function __construct($id, $nombre, $departamento) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->departamento = $departamento;
    }

    // getters
    public function getId() {
        return $this->id;
    }

    public function getNombre() {
        return $this->nombre;
    }
    
    public function getDepartamento() {
        return $this->departamento;
    }
    
    // setters
    public function setId($id) {
        $this->id = $id;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }
    
    public function setDepartamento($departamento) {
        $this->departamento = $departamento;
    }

}

?>