<?php 
    require "../modelo/config.php";
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Añadir departamento</title>
        <link rel="stylesheet" href="../css/index.css">
        <link rel="stylesheet" href="../css/estilos.css">
    </head>
    <body>
        <?php include "header.php"; ?>
        
        <h2>Introduzca los datos del nuevo departamento</h2>
        <form method="POST" action="../controlador/alta_departamento.php" >
            
            <label for="nombre">Nombre:</label>
            <input size="50" type="text" name="nombre" required placeholder="Nombre del departamento" title="Por favor, introduce un nombre correcto. Sólo letras y espacios" pattern="[a-zA-Z\W]+" />
            <br/><br/>
            
            <label for="ubicacion">Ubicación:</label>
            <input size="50" type="text" name="ubicacion" required placeholder="Valencia" title="Por favor, introduce un nombre correcto. Sólo letras, números, espacios y comas" pattern="[a-zA-Z0-9,\W]+" />
            <br/><br/>
            
            <input type="submit" name="Enviar" value="Enviar" />
            <input type="reset" name="Borrar" value="Borrar" />
            <br/><br/>

        </form>
        
        <?php include "footer.php"; ?>
    </body>
</html>