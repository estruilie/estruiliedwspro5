<?php 
    require "../modelo/config.php";
    include_once "../modelo/modelo.php";
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Añadir trabajador</title>
        <link rel="stylesheet" href="../css/index.css">
        <link rel="stylesheet" href="../css/estilos.css">
    </head>
    <body>
        <?php include "header.php"; ?>
        
        <h2>Introduzca los datos del nuevo trabajador</h2>
        <form method="POST" action="../controlador/alta_trabajador.php" >
            
            <label for="id">Identificador:</label>
            <input size="4" type="text" name="id" required placeholder="0" title="Por favor, introduce sólo el identificador numérico" pattern="[0-9]+" />
            <br/><br/>

            <label for="nombre">Nombre:</label>
            <input size="50" type="text" name="nombre" required placeholder="Nombre y apellidos" title="Por favor, introduce un nombre correcto. Sólo letras, espacios y comas" pattern="[a-zA-Z,\W]+" />
            <br/><br/>
            
            <label for="departamento">Departamento:</label>
            <select name="departamento">
                <?php
                    $modelo = new modelo();
                    $departamentos = $modelo->leerDepartamentos();
                    if ( $departamentos ) {
                        foreach ($departamentos as $d) {
                            echo "<option value='" . $d->getNombre()."'>" . $d->getNombre() . "</option>";
                        }
                    }
                    else
                        echo "<option value=''>Todavía no existe ningún departamento</option>";
                ?>
            </select>
            <br/><br/>
            
            <input type="submit" name="Enviar" value="Enviar" />
            <input type="reset" name="Borrar" value="Borrar" />
            <br/><br/>

        </form>
        
        <?php include "footer.php"; ?>
    </body>
</html>