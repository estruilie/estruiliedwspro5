<?php 
    require "../modelo/config.php";
    include("../modelo/modelo.php");
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>departamentos.php</title>
        <link rel="stylesheet" href="../css/index.css">
        <link rel="stylesheet" href="../css/estilos.css">
    </head>
    <body>
        <?php include "header.php"; ?>
   
        <div class="tablas">
            <h2> DEPARTAMENTOS EXISTENTES: </h2>
            <?php

                echo '<table border="2">';
                echo '<tr>';
                echo '<th>Nombre Departamento</th>';
                echo '<th>Ubicación</th>';
                echo '</tr>';

                $modelo = new Modelo();
                $departamentos = $modelo->leerDepartamentos();

                if (count($departamentos) != 0) {

                    foreach ($departamentos as $d) {
                        echo "<tr>";
                        echo "<td>" . $d->getNombre() . "</td>";
                        echo "<td>" . $d->getUbicacion() . "</td>";
                        echo "</tr>";
                    }
                }

                echo "</table>";
                echo "<br/>";
                
           ?>
            <h2><a href="alta_departamento.php"> Dar de alta un nuevo departamento </a></h2>
        </div>
        
        
        <?php include "footer.php"; ?>
    </body>
</html>