<?php
include_once '../modelo/config.php';

/* 
 * PIE DE PÁGINA INCLUÍDO EN TODAS LAS PÁGINAS
 */

function enInicio() {
    // se utiliza para redirigir a la página de inicio en caso de encontrarse en una distinta

    // $_SERVER['PHP_SELF'] devuelve ruta actual. Pej: /dwst5p1/index.php
    $ini = strpos($_SERVER['PHP_SELF'],"index.php"); //devuelve la posición de la cadena buscada. False si no encontrada

    // si no estamos en index.php redirigimos a index.php
    if ( $ini === false)
        echo "<a href = 'index.php'> &gt;&gt;Inicio </a>";
}

echo "<div class='pie1'>";
echo "<p>" . Config::$autor . " - " . Config::$fecha . " - " . Config::$curso . "</p>";
echo "<p>" . Config::$empresa . " - <a href='../doc/Documentacion.pdf' target='_blank' >Ver Documentacion.pdf</a>
         - <a href='../doc/Documentacion.odt' target='_blank' >Ver Documentacion.odt</a></p>";
echo "</div>";
echo "<div class='pie2'>";
enInicio();
echo "</div>";
?>