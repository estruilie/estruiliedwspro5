<?php require "../modelo/config.php"; ?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Evaluable T5 - Estrella Ruiz Liébana</title>
        <link rel="stylesheet" href="../css/index.css">
    </head>
    <body>
        <?php include "header.php"; ?>
        <h2> ELIGE UNA OPCIÓN: </h2>
        <div class="opciones">
            <a href="departamentos.php"><h2>GESTIONAR DEPARTAMENTOS</h2></a>
        </div>
        <div class="opciones">
            <a href="trabajadores.php"><h2>GESTIONAR TRABAJADORES</h2></a>
        </div>
        <?php include "footer.php"; ?>
    </body>
</html>
