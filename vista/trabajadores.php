<?php 
    require "../modelo/config.php";
    include_once "../modelo/modelo.php";
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>trabajadores.php</title>
        <link rel="stylesheet" href="../css/index.css">
        <link rel="stylesheet" href="../css/estilos.css">
    </head>
    <body>
        <?php include "header.php"; ?>
   
        <div class="tablas">
            <h2> TRABAJADORES EXISTENTES: </h2>
            <?php

                echo '<table border="2">';
                echo '<tr>';
                echo '<th>ID Trabajador</th>';
                echo '<th>Nombre Trabajador</th>';
                echo '<th>Departamento Asociado</th>';
                echo '</tr>';

                $modelo = new Modelo();
                $trabajadores = $modelo->leerTrabajadores();

                if ( count($trabajadores) > 0 ) {

                    foreach ( $trabajadores as $t ) {
                        echo "<tr>";
                        echo "<td>" . $t->getId() . "</td>";
                        echo "<td>" . $t->getNombre() . "</td>";
                        echo "<td>" . $t->getDepartamento() . "</td>";
                        echo "</tr>";
                    }
                }

                echo "</table>";
                echo "<br/>";
                
           ?>
            <h2><a href="alta_trabajador.php"> Dar de alta un nuevo trabajador </a></h2>
        </div>
        
        
        <?php include "footer.php"; ?>
    </body>
</html>